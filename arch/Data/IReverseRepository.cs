﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace arch.Data
{
	public interface IReverseRepository
	{
		IEnumerable<S> GetRelated<S>(Expression<Func<S, bool>> whereClause) where S : BaseEntity, new();
	}
}
