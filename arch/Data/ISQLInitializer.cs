﻿using System;
using SQLite.Net;

namespace arch
{
	public interface ISQLInitializer
	{
		SQLiteConnection GetConnection();
	}
}
