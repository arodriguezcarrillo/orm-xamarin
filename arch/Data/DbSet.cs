﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace arch.Data
{
	public class DbSet<T> : List<T> where T : BaseEntity, new()
	{
		private BaseEntityCollection<T> _local;
		private Func<IEnumerable<T>> _method;

		public DbSet(Func<IEnumerable<T>> method)
		{
			this._method = method;
		}

		public ObservableCollection<T> Local
		{
			get
			{
				if (_local == null &&
					this._method != null)
				{
					_local = new BaseEntityCollection<T>(
						this._method()
					);
				}

				return _local;
			}
		}

	}
}
