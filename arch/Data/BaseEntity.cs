﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SQLite.Net.Attributes;

namespace arch.Data
{
	public abstract class BaseEntity : INotifyPropertyChanged
	{
		public BaseEntity()
		{
			InitializeDBSet();
		}



		private void InitializeDBSet()
		{
			var dbsetType = typeof(DbSet<>);
			var properties = this.GetType().GetRuntimeProperties();
			foreach (var property in properties)
			{
				if (property.PropertyType.IsConstructedGenericType &&
				    property.PropertyType.GetGenericTypeDefinition() == dbsetType)
				{
					var declaringType = property.PropertyType.GenericTypeArguments[0];
					var newType = dbsetType.MakeGenericType(new Type[] { declaringType });
					//TODO: Activar
					/*property.SetValue(this, 
					                  Activator.CreateInstance(newType, 
					                                           new object[] { })
					                 );*/
				}
			}
		}

		public virtual ISpecification IdentifiedBy()
		{
			return new BaseEntitySpecification(this.Id);
		}


		[PrimaryKey]
		public int Id
		{
			get;
			set;
		}

		protected void SendPropertyChanged([CallerMemberName]string propertyName = "")
		{
			if (this.PropertyChanged != null)
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		public abstract void Update(object data);

		public event PropertyChangedEventHandler PropertyChanged;
	}
}
