﻿using System;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System.Threading.Tasks;
using SQLiteNetExtensions.Attributes;
using System.Collections.ObjectModel;

namespace arch.Data
{
	public abstract class Repository<T> : IReverseRepository where T : BaseEntity, new()
	{
		protected SQLiteConnection _connection;
		private static BaseEntityCollection<T> _local;

		public ObservableCollection<T> Local
		{
			get
			{
				if (_local == null)
					_local = new BaseEntityCollection<T>(
						this.GetAll().ToList()
					);

				return _local;
			}
		}
		
		public Repository()
		{
			if (_connection == null)
			{
				var initializer = Xamarin.Forms.DependencyService.Get<ISQLInitializer>();

				if (initializer != null)
					this._connection = initializer.GetConnection();

				this.CreateTables();
			}
		}

		private void CreateTables()
		{
			this._connection.CreateTable<T>(SQLite.Net.Interop.CreateFlags.AutoIncPK);
			CreateTablesForProperties();
		}

		private void CreateTablesForProperties()
		{
			var type = typeof(T);
			var properties = type.GetRuntimeProperties();
			foreach (var property in properties)
			{
				var mustCreateTable = property.CustomAttributes.Any(a => 
                          a.AttributeType == typeof(OneToOneAttribute) || 
                          a.AttributeType == typeof(OneToManyAttribute) ||
                          a.AttributeType == typeof(ManyToOneAttribute)
             	);

				if (mustCreateTable)
				{
					Type typeOfTableToCreate;
					if (property.PropertyType.IsConstructedGenericType)
						typeOfTableToCreate = property.PropertyType.GenericTypeArguments[0];
					else
						typeOfTableToCreate = property.PropertyType;

					CreateTableForProperty(typeOfTableToCreate);
				}
			}
		}

		private void CreateTableForProperty(Type type)
		{
			var connectionType = this._connection.GetType();
			var method = connectionType.GetRuntimeMethods().FirstOrDefault(m => m.Name == "CreateTable");
			var genericMethod = method.MakeGenericMethod(new Type[] { type });
			genericMethod.Invoke(this._connection, new object[] { SQLite.Net.Interop.CreateFlags.AutoIncPK });
		}

		public IQueryable<T> GetAll()
		{
			return this._connection.Table<T>().AsQueryable();
		}

		public IQueryable<T> GetAll(Expression<Func<T, bool>> whereClause)
		{
			return this._connection.Table<T>().Where(whereClause).AsQueryable();
		}

		internal IQueryable<S> GetBySpec<S>(ISpecification spec) where S : BaseEntity, new()
		{
			Func<S, bool> expr = spec.SatisfiedBy().Compile() as Func<S, bool>;
			return this._connection.Table<S>().Where(expr).AsQueryable();
		}

		public T GetSingle(Expression<Func<T, bool>> whereClause)
		{
			return this._connection.Table<T>().Where(whereClause).FirstOrDefault();
		}

		internal S GetSingle<S>(ISpecification spec) where S : BaseEntity, new()
		{
			
			return this._connection.Table<S>().Where(expr).FirstOrDefault();
		}

		public bool Delete(T item)
		{
			return this._connection.Delete(item) > 0;
		}

		private void InternalUpdate<S>(S item) where S : BaseEntity
		{
			Func<S, bool> expr = item.IdentifiedBy().SatisfiedBy().Compile() as Func<S, bool>;
			if (this._connection.Table<S>().Where(expr).Count() > 0)
				this._connection.Update(item);
			else
				this._connection.InsertWithChildren(item);
		}

		public void Update(T item)
		{
			try
			{
				this.InternalUpdate(item);
				_local.Add(item);
				//this._connection.Commit();
			}
			catch (Exception excep)
			{
				//this._connection.Rollback();
			}
		}

		public IEnumerable<S> GetRelated<S>(Expression<Func<S, bool>> whereClause) where S : BaseEntity, new()
		{
			return this._connection.Table<S>().Where(whereClause);
		}
	}
}
