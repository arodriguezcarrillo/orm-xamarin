﻿using System;
using orm.data;
using Xamarin.Forms;

namespace orm
{
	public partial class ListPage : ContentPage
	{
		public ListPage()
		{
			InitializeComponent();

			this.lvdata.ItemsSource = ClientsRepository.Current.Local;
			this.lvdata.ItemTapped += (sender, e) =>
			{
				var item = e.Item as Client;
				if (item != null)
				{
					this.Navigation.PushAsync(new DetailsPage(item.Id));
				}
			};
		}

		public void NewClicked(object sender, EventArgs e)
		{
			this.Navigation.PushAsync(new DetailsPage());
		}
	}
}
