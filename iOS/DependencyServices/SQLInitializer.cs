﻿using System;
using System.IO;
using arch;
using orm.iOS;
using SQLite.Net;

[assembly:Xamarin.Forms.Dependency(typeof(SQLInitializer))]
namespace orm.iOS
{
	public class SQLInitializer : ISQLInitializer
	{
		public SQLInitializer()
		{
		}

		public SQLiteConnection GetConnection()
		{
			var sqliteFilename = "orm.db3";
			string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); 
			string libraryPath = Path.Combine(documentsPath, "..", "Library");
			var path = Path.Combine(libraryPath, sqliteFilename);
			var conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS(),
			                                           path);
			return conn;
		}
	}
}
