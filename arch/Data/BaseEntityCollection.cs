﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using arch.Data;
using System.Linq.Expressions;

namespace arch
{
	internal class BaseEntityCollection<T> : ObservableCollection<T> where T : BaseEntity, new()
	{
		public BaseEntityCollection()
		{
		}

		public BaseEntityCollection(IEnumerable<T> data) : base(data)
		{
		}

		private bool Exists(int id)
		{
			return this.Any(i => i.Id == id);
		}

		private void Update(T item)
		{
			var expr = item.IdentifiedBy().SatisfiedBy().Compile() as Func<T, bool>;
			var itemToUpdate = this.FirstOrDefault(expr);
			if (itemToUpdate != null)
				itemToUpdate.Update(item);
		}

		public void AddRange(IEnumerable<T> items)
		{
			foreach (var item in items)
			{
				this.Add(item);
			}
		}

		public new void Add(T item)
		{
			if (this.Exists(item.Id))
			{
				this.Update(item);
			}
			else
				base.Add(item);
		}
	}
}
