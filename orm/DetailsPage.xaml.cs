﻿using System;
using System.Collections.Generic;
using orm.data;
using Xamarin.Forms;

namespace orm
{
	public partial class DetailsPage : ContentPage
	{
		Client _client;

		public DetailsPage(int? id = null)
		{
			InitializeComponent();

			if (id.HasValue)
				this._client = ClientsRepository.Current.GetSingle(i => i.Id == id);
			else
				this._client = new Client();

			this.BindingContext = this._client;
		}

		public void SaveClicked(object sender, EventArgs e)
		{
			ClientsRepository.Current.Update(this._client);
			this.Navigation.PopAsync();
		}

		public void NewOrderClicked(object sender, EventArgs e)
		{
			var orderToAdd = new Order();
			
			this._client.Orders.Add(orderToAdd);
			this.Navigation.PushAsync(new OrderDetails(orderToAdd));
		}
	}
}
