﻿using System;
using arch.Data;

namespace orm.data
{
	public class ClientsRepository : Repository<Client>
	{

		static ClientsRepository _current;
		public static ClientsRepository Current
		{
			get
			{
				if (_current == null)
					_current = new ClientsRepository();

				return _current;
			}
		}

	}
}
