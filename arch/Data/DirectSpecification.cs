﻿using System;
using arch.Data;
using System.Linq.Expressions;

namespace arch
{
	public interface ISpecification
	{
		LambdaExpression SatisfiedBy();
	}

	public class BaseEntitySpecification : ISpecification
	{
		int _id;

		public BaseEntitySpecification(int id)
		{
			this._id = id;
		}


		public LambdaExpression SatisfiedBy()
		{
			Expression<Func<BaseEntity, bool>> expr = y => y.Id == this._id;
			LambdaExpression l = expr;

			return l;
		}
	}

	public class DirectSpecification<T> : ISpecification
	{
		Func<T, bool> _expr;

		public DirectSpecification(Func<T, bool> expr)
		{
			this._expr = expr;
		}


		public LambdaExpression SatisfiedBy()
		{
			Expression<Func<T, bool>> expr = x => this._expr(x);
			LambdaExpression l = expr;
			return l;
		}
	}
}
