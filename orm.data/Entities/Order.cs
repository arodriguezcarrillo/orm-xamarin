﻿using System;
using arch.Data;
using SQLiteNetExtensions.Attributes;

namespace orm.data
{
	public class Order : BaseEntity
	{
		string _code;
		public string Code
		{
			get
			{
				return this._code;
			}
			set
			{
				this._code = value;
				SendPropertyChanged();
			}
		}

		float _amount;
		public float Amount
		{
			get
			{
				return this._amount;
			}
			set
			{
				this._amount = value;
				SendPropertyChanged();
			}
		}

		[ForeignKey(typeof(Client))]
		public int ClientId { get; set; }

		[ManyToOne]
		public Client Client { get; set;}

		public override void Update(object data)
		{
			var item = data as Order;
			if (item != null)
			{
				this.Amount = item.Amount;
				this.ClientId = item.ClientId;
				this.Code = item.Code;
			}
		}
	}
}
