﻿using System;
using System.Collections.Generic;
using arch;
using arch.Data;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace orm.data
{
	public class Client : BaseEntity
	{
		string _name;
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
				SendPropertyChanged();
				SendPropertyChanged("CompleteName");
			}
		}

		string _surname;
		public string Surname
		{
			get
			{
				return this._surname;
			}
			set
			{
				this._surname = value;
				SendPropertyChanged();
				SendPropertyChanged("CompleteName");
			}
		}

		string _nif;
		public string Nif
		{
			get
			{
				return this._nif;
			}
			set
			{
				this._nif = value;
				SendPropertyChanged();
			}
		}

		[Ignore]
		public string CompleteName
		{
			get
			{
				return String.Format("{0} {1}", this.Name, this.Surname);
			}
		}

		[OneToMany(CascadeOperations = CascadeOperation.All)]
		public DbSet<Order> Orders { get; set; }

		public override void Update(object data)
		{
			var item = data as Client;
			if (item != null)
			{
				this.Name = item.Name;
				this.Nif = item.Nif;
				this.Surname = item.Surname;
			}
		}

		public override ISpecification IdentifiedBy()
		{
			return new DirectSpecification<Client>(i => i.Nif == this.Nif);
		}
	}
}
