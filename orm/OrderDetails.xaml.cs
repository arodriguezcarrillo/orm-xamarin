﻿using System;
using System.Collections.Generic;
using orm.data;
using Xamarin.Forms;

namespace orm
{
	public partial class OrderDetails : ContentPage
	{
		
		public OrderDetails(Order order)
		{
			InitializeComponent();
			this.BindingContext = order;
		}

		public void SaveClicked(object sender, EventArgs e)
		{
			this.Navigation.PopAsync();
		}
	}
}
